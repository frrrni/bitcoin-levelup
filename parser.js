var fs = require('fs');





function hex(buffer) {
  return buffer.toString('hex');
}


var stream = fs.createReadStream('D:\\bitcoin-blockchain\\blocks\\blk00000.dat');

stream.on('readable', function() {

  console.log("Magic Number:\t %s", uint4(stream).toString('hex'));
  console.log("Blocksize:\t %s", uint4(stream).toString('hex'));

  console.log("Block Header");
  console.log("Version:\t %s", uint4(stream).toString('hex'));
  console.log("Previous Hash\t %s", hashStr(hash32(stream)));
  console.log("Merkle Root\t %s", hashStr(hash32(stream)));
  console.log("Time\t\t %s", time(stream));
  console.log("Difficulty\t %s", uint4(stream));
  console.log("Nonce\t\t %s", uint4(stream));

  console.log("Tx Count\t %d", varint(stream));

  console.log("Version Number\t %s", uint4(stream));
  console.log("Inputs\t\t %s", varint(stream));
  console.log("Previous Tx\t %s", hashStr(hash32(stream)));
  console.log("Prev Index \t %d", uint4(stream));
  var script_len = varint(stream);
  console.log("Script Length\t %d", script_len);
  var script_sig = stream.read(script_len);
  console.log("ScriptSig\t %s", hashStr(script_sig));
  console.log("ScriptSig\t %s", hashStr(script_sig));
  console.log("Seq Num\t\t %8x", uint4(stream));

  console.log("Outputs\t\t %s", varint(stream));
  console.log("Value\t\t %s", str((uint8(stream) * 1.0) / 100000000.00));
  script_len = varint(stream);
  console.log("Script Length\t %d", script_len);
  var script_pubkey = stream.read(script_len);
  console.log("Script Pub Key\t %s", hashStr(script_pubkey));
  console.log("Lock Time %8x", uint4(stream));
});