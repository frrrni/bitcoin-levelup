module.exports = {
  uint1: function uint1(stream) {
    //return ord(stream.read(1))
    return stream.read(1);
  },
  uint2: function uint2(stream) {
    //return struct.unpack('H', stream.read(2))[0]
    return stream.read(2);

  },
  uint4: function uint4(stream) {
    //return struct.unpack('I', stream.read(4))[0]
    return stream.read(4);

  },
  uint8: function uint8(stream) {
    //return struct.unpack('Q', stream.read(8))[0]
    return stream.read(8);

  },
  hash32: function hash32(stream) {
    var buf = stream.read(32);
    var buf2 = new Buffer(32);
    for(var i = 0; i < 32; i++) {
      buf2[i] = buf[31 - i];
    }
    return buf2;
  },
  time: function time(stream) {
    return uint4(stream);
  },
  varint: function varint(stream) {
    var size = this.uint1(stream);
    var sizeInt = size.readUInt8();
    if (sizeInt < 253) return sizeInt;
    if (sizeInt == 253) return this.uint2(stream).readUInt8();
    if (sizeInt == 254) return this.uint4(stream).readUInt8();
    if (sizeInt == 255) return this.uint8(stream).readUInt8();
    return -1;
  },
  hashStr: function hashStr(bytebuffer) {
    //return ''.join(('%x' % ord(a)) for a in bytebuffer)
    return bytebuffer.toString('hex');
  }
};