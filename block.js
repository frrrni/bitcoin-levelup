var bt = require('./blocktools');

function BlockHeader(blockchain) {
  this.version = bt.uint4(blockchain);
  this.previousHash = bt.hash32(blockchain);
  this.merkleHash = bt.hash32(blockchain);
  this.time = bt.uint4(blockchain);
  this.bits = bt.uint4(blockchain);
  this.nonce = bt.uint4(blockchain);
}


BlockHeader.prototype.toString = function() {
  /*
  print "Version:\t %d" % self.version
  print "Previous Hash\t %s" % hashStr(self.previousHash)
  print "Merkle Root\t %s" % hashStr(self.merkleHash)
  print "Time\t\t %s" % str(self.time)
  print "Difficulty\t %8x" % self.bits
  print "Nonce\t\t %s" % self.nonce
  */
};

exports.BlockHeader = BlockHeader;



exports.Block = function Block(blockchain) {
  this.magicNum = bt.uint4(blockchain);
  this.blocksize = bt.uint4(blockchain);
  this.blockHeader = new BlockHeader(blockchain);
  //this.txCount = parseInt(bt.varint(blockchain).toString('hex'), 16);
  this.txCount = bt.varint(blockchain);
  this.txs = [];
  for (var i = 0; i < this.txCount; i++) {
    this.txs.push(new Tx(blockchain))
  }
};
/*
def toString(self):
print ""
print "Magic No: \t%8x" % self.magicNum
print "Blocksize: \t", self.blocksize
print ""
print "#" * 10 + " Block Header " + "#" * 10
self.blockHeader.toString()
print
print "##### Tx Count: %d" % self.txCount
for t in self.Txs:
t.toString()
*/

function Tx(blockchain) {
    this.version = bt.uint4(blockchain);
    this.inCount = bt.varint(blockchain);
    this.inputs = [];
    for(var i = 0; i < this.inCount; i++) {
      this.inputs.push(new TxInput(blockchain));
    }


    this.outCount = bt.varint(blockchain);
    this.outputs = [];
    for(i = 0; i < this.outCount; i++) {
      this.outputs.push(new TxOutput(blockchain));
    }

    this.lockTime = bt.uint4(blockchain);
}

/*
  def toString(self):
    print ""
    print "=" * 10 + " New Transaction " + "=" * 10
    print "Tx Version:\t %d" % self.version
    print "Inputs:\t\t %d" % self.inCount
    for i in self.inputs:
    i.toString()

    print "Outputs:\t %d" % self.outCount
    for o in self.outputs:
    o.toString()
    print "Lock Time:\t %d" % self.lockTime

*/
function TxInput(blockchain) {
  this.prevhash = bt.hash32(blockchain);
  this.txOutId = bt.uint4(blockchain);
  this.scriptLen = bt.varint(blockchain);
  this.scriptSig = blockchain.read(this.scriptLen);
  this.seqNo = bt.uint4(blockchain);
}
/*
  def toString(self):
    print "Previous Hash:\t %s" % hashStr(self.prevhash)
    print "Tx Out Index:\t %8x" % self.txOutId
    print "Script Length:\t %d" % self.scriptLen
    print "Script Sig:\t %s" % hashStr(self.scriptSig)
    print "Sequence:\t %8x" % self.seqNo
*/

function TxOutput(blockchain) {
  this.value = bt.uint8(blockchain).readUIntLE(0, 8);
  this.scriptLen = bt.varint(blockchain);
  this.pubkey = blockchain.read(this.scriptLen);
}
/*
  def toString(self):
    print "Value:\t\t %d" % self.value
    print "Script Len:\t %d" % self.scriptLen
    print "Pubkey:\t\t %s" % hashStr(self.pubkey)
*/