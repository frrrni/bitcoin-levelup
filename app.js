var levelup = require('levelup')

var exampleKeys = {
	someFile: (new Buffer('f')).toString('hex') + '57010000',
	someBlock: '623793df0401ece34fe57c99bd1d6556cb5c1d27a30b69a12ecc07000000000000'
}

levelup('./index', {
	keyEncoding: 'hex',
  valueEncoding: 'hex'
}, function(err, db) {
	if (err) throw err;// 3) fetch by key
/*
db.createKeyStream()
  .on('data', function (data) {
    console.log('key=', data)
  });
*/
  db.get('42', function (err, value) {
    if (err) return console.log('Ooops!', err) // likely the key was not found

    // ta da!
    console.log(value);
  })
});

